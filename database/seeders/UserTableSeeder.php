<?php

namespace Database\Seeders;

use App\Models\Level;
use App\Models\Student;
use App\Models\User;
use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        {
            $users = [
                [ 'name' => 'user 1', 'email' => "admin@info.con", 'password' => bcrypt('12345678')],
                [ 'name' => 'user 2', 'email' => "user@info.con", 'password' => "$2y$10\$TVXt40eGUJ9HzD/ajX62yeOOgtTk5mJJkuVzLvKaccSwEMWaTsLKK"],
            ];
            foreach ($users as $user){
                User::create($user);
            }
        }
    }
}
