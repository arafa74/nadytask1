<?php

namespace Database\Seeders;

use App\Models\Level;
use Illuminate\Database\Seeder;

class LevelTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        {
            $levels = [
                [ 'name' => 'level1', 'number' => 1, 'description' => 'level1 Desc' ],
                [ 'name' => 'level2', 'number' => 2, 'description' => 'level2 Desc' ],
                [ 'name' => 'level3', 'number' => 3, 'description' => 'level3 Desc' ],
                [ 'name' => 'level4', 'number' => 4, 'description' => 'level4 Desc' ],
            ];
            foreach ($levels as $level){
                Level::create($level);
            }
        }
    }
}
