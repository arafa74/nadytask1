<?php

namespace Database\Seeders;

use App\Models\GradeItem;
use Illuminate\Database\Seeder;

class LevelTableSeederGrageItemTablSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        {
            {
                $gradeItems = [
                    [ 'name' => 'practical exam', 'max_degree' => null ],
                    [ 'name' => 'oral exam', 'max_degree' => null ],
                    [ 'name' => 'midterm exam', 'max_degree' => null ],
                    [ 'name' => 'final exam', 'max_degree' => null ],
                ];
                foreach ($gradeItems as $gradeItem){
                    GradeItem::create($gradeItem);
                }
            }
        }
    }
}
