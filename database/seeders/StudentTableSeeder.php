<?php

namespace Database\Seeders;

use App\Models\Level;
use App\Models\Student;
use Illuminate\Database\Seeder;

class StudentTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        {
            $students = [
                [ 'name' => 'stu1', 'code' => 123, 'email' => 'stu1@info.com', 'level_id' => 1,'data_of_brith' => '2000-12-12' ],
                [ 'name' => 'stu2', 'code' => 345, 'email' => 'stu4@info.com', 'level_id' => 2,'data_of_brith' => '2005-12-12' ],
                [ 'name' => 'stu3', 'code' => 678, 'email' => 'stu3@info.com', 'level_id' => 3,'data_of_brith' => '2005-12-12' ],
                [ 'name' => 'stu4', 'code' => 900, 'email' => 'stu2@info.com', 'level_id' => 4,'data_of_brith' => '2005-12-12' ],
            ];
            foreach ($students as $student){
                Student::create($student);
            }
        }
    }
}
