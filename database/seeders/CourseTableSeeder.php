<?php

namespace Database\Seeders;

use App\Models\Course;
use App\Models\Level;
use App\Models\Student;
use Illuminate\Database\Seeder;

class CourseTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        {
            $courses = [
                [ 'name' => 'Course 1', 'code' => 123,'description' => 'description Course 1' ],
                [ 'name' => 'Course 2', 'code' => 345, 'description' => 'description Course 2'],
                [ 'name' => 'Course 3', 'code' => 678, 'description' => 'description Course 3'],
                [ 'name' => 'Course 4', 'code' => 900, 'description' => 'description Course 4'],
            ];
            foreach ($courses as $course){
                Course::create($course);
            }
        }
    }
}
