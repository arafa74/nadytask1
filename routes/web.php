<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Route::get('/courses', [\App\Http\Controllers\CourseController::class, 'index'])->name('courses');
Route::get('/students', [\App\Http\Controllers\StudentController::class, 'index'])->name('students');
Route::get('/delete_student/{id}', [\App\Http\Controllers\StudentController::class, 'delete'])->name('delete_student');
Route::get('/students_create', [\App\Http\Controllers\StudentController::class, 'create'])->name('students_create');
Route::post('/students_store', [\App\Http\Controllers\StudentController::class, 'store'])->name('students_store');
Route::post('/search_students', [\App\Http\Controllers\StudentController::class, 'search'])->name('search_students');
Route::post('/search_courses', [\App\Http\Controllers\CourseController::class, 'search'])->name('search_courses');
Route::get('/course/{id}', [\App\Http\Controllers\CourseController::class, 'show'])->name('show_courses');
Route::get('/remove_from_course/{id}/{course_id}', [\App\Http\Controllers\CourseController::class, 'removeFromCourse'])->name('remove_from_course');
Route::post('/add_student_to_course', [\App\Http\Controllers\CourseController::class, 'addStudentToCourse'])->name('add_student_to_course');

