<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreStudent;
use App\Models\Level;
use App\Models\Student;
use Database\Seeders\LevelTableSeeder;
use Illuminate\Http\Request;

class StudentController extends Controller
{
    public function index()
    {
        $students  = Student::all();
        return view('students' , compact('students'));
    }

    public function create()
    {
        $levels  = Level::all();
        return view('student_create' , compact('levels'));
    }

    public function store(Request $request)
    {
        Student::create($request->all());
        return redirect()->route('students')->with('success', 'Added Successfully');;
    }

    public function search(Request $request)
    {
        $students = Student::when($request->search_key, function ($q) use ($request) {
            $q->where('email', $request->search_key)
                ->orWhere('code',$request->search_key)
                ->orWhere('name', 'LIKE', "%{$request->search_key}%");
        })->get();
        return view('students' , compact('students'));
    }

    public function delete($id)
    {
        Student::find($id)->delete();
        return back()->with('success', 'deleted Successfully');;
    }
}
