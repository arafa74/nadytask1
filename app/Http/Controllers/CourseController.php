<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreStudent;
use App\Models\Course;
use App\Models\Level;
use App\Models\Student;
use App\Models\StudentCourse;
use Database\Seeders\LevelTableSeeder;
use Illuminate\Http\Request;

class CourseController extends Controller
{
    public function index()
    {
        $courses  = Course::all();
        return view('courses' , compact('courses'));
    }

    public function search(Request $request)
    {
        $courses = Course::when($request->search_key, function ($q) use ($request) {
            $q->where('code',$request->search_key)
                ->orWhere('name', 'LIKE', "%{$request->search_key}%");
        })->get();
        return view('courses' , compact('courses'));
    }

    public function show($id)
    {
        $course = Course::find($id);
        $students = $course->students;
        $allStudents = Student::all();
        return view('course_details' , compact('course' , 'students','allStudents'));
    }

    public function removeFromCourse($id,$course_id)
    {
        $studentInCourse = StudentCourse::where('student_id' , $id)->where('course_id',$course_id)->first();
        $studentInCourse->delete();
        return back()->with('success' , 'removed successfully');
    }

    public function addStudentToCourse(Request $request)
    {
        if ( StudentCourse::where('student_id' , $request->student_id)->where('course_id',$request->course_id)->exists()){
            return back()->with('error', 'Added before To Course');
        }
        StudentCourse::create($request->all());
        return back()->with('success', 'Added Successfully');
    }
}
