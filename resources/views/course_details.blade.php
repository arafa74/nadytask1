@extends('layouts.app')

@section('content')

    <style>
        table, th, td {
            border:1px solid black;
        }

        .action_btn {
            width: 200px;
            margin: 0 auto;
            display: inline;}
        .conoflinks {
            justify-content: center;
            display: flex;
            align-items: center;
        }
        .navbar-brand{
            margin: 10px;
            font-size: 20px;
            font-weight: bold;
        }
    </style>
    @if (\Session::has('success'))
        <div class="conoflinks">
            <div class="col-10">
                <div class="alert alert-success" role="alert">
                    {!! \Session::get('success') !!}
                    <span class="closebtn" onclick="this.parentElement.style.display='none';">&times;</span>
                </div>
            </div>
        </div>
    @endif


    @if (\Session::has('error'))
        <div class="conoflinks">
            <div class="col-10">
                <div class="alert alert-danger" role="alert">
                    {!! \Session::get('error') !!}
                    <span class="closebtn" onclick="this.parentElement.style.display='none';">&times;</span>
                </div>
            </div>
        </div>
    @endif

    <div class="action_btn">
        <div class="conoflinks">
            <form method="post" action="{{ route('add_student_to_course') }}">
                @csrf
                <label for="student_id">choose Student To add To Course:</label><br>
                <input type="hidden" name="course_id" value="{{ $course->id }}">
                <select name="student_id" id="studentl_id" required>
                    @foreach($allStudents as $student)
                        <option value="{{ $student->id }}">{{ $student->name }}</option>
                    @endforeach
                </select>
                <button type="submit">submit</button>
            </form>
        </div>
    </div>
    <br>

    <div class="conoflinks">
        <table style="width:60%" >
            <tr>
                <th>student name</th>
                <th>student code</th>
                <th>delete</th>
            </tr>
            @foreach($students as $student)
                <tr>
                    <td>{{ $student->name }}</td>
                    <td>{{ $student->code }}</td>
                    <td>
                        <a href="{{ url('remove_from_course/'  . $student->id .'/'. $course->id) }}" class="btn btn-danger btn-ok">Remove From Course</a>
                    </td>
                </tr>
            @endforeach
        </table>
    </div>
@endsection
