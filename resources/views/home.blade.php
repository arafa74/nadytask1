@extends('layouts.app')

@section('content')
    <style>
        .buttons {
            width: 200px;
            margin: 0 auto;
            display: inline;}

        .action_btn {
            width: 200px;
            margin: 0 auto;
            display: inline;}
        .conoflinks {
            justify-content: center;
            display: flex;
            align-items: center;
        }
        .navbar-brand{
            margin: 10px;
            font-size: 20px;
            font-weight: bold;
        }
    </style>

{{--    <div class="container">--}}
{{--        <div class="row justify-content-center">--}}
{{--            <div class="col-md-8">--}}
{{--                <div class="card">--}}

{{--                    <div class="card-body">--}}
{{--                        @if (session('status'))--}}
{{--                            <div class="alert alert-success" role="alert">--}}
{{--                                {{ session('status') }}--}}
{{--                            </div>--}}
{{--                        @endif--}}

{{--                    </div>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--        </div>--}}
{{--    </div>--}}


<div class="buttons">

    <div class="action_btn">
        <div class="text-center conoflinks">
            <a class="navbar-brand" href="{{ url('/students') }}">
                show students
            </a>
            <a class="navbar-brand" href="{{ url('/courses') }}">
                show courses
            </a>
        </div>

        <p id="saved"></p>
    </div>

</div>
@endsection
