@extends('layouts.app')

@section('content')

    <style>
        table, th, td {
            border:1px solid black;
        }

        .action_btn {
            width: 200px;
            margin: 0 auto;
            display: inline;}
        .conoflinks {
            justify-content: center;
            display: flex;
            align-items: center;
        }
        .navbar-brand{
            margin: 10px;
            font-size: 20px;
            font-weight: bold;
        }
    </style>


    <div class="conoflinks">
        <form method="post" action="{{ route('students_store') }}">
            @csrf
            <label for="fname">First name:</label><br>
            <input type="text" id="name" name="name" required><br>

            <label for="code">Code:</label><br>
            <input type="number" id="code" name="code" required><br><br>

            <label for="date_of_birth">date of birth:</label><br>
            <input type="date" id="date" name="data_of_brith" required><br><br>

            <label for="email">email:</label><br>
            <input type="email" id="email" name="email" required><br><br>

            <label for="Level">Level:</label><br>
            <select name="level_id" id="level_id" required>
                @foreach($levels as $level)
                <option value="{{ $level->id }}">{{ $level->name }}</option>
                @endforeach
            </select>
            <button type="submit">submit</button>
        </form>
    </div>
@endsection
