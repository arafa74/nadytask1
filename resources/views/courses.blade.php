@extends('layouts.app')

@section('content')

    <style>
        table, th, td {
            border:1px solid black;
        }

        .action_btn {
            width: 200px;
            margin: 0 auto;
            display: inline;}
        .conoflinks {
            justify-content: center;
            display: flex;
            align-items: center;
        }
        .navbar-brand{
            margin: 10px;
            font-size: 20px;
            font-weight: bold;
        }
    </style>
    @if (\Session::has('success'))
        <div class="conoflinks">
            <div class="col-10">
                <div class="alert alert-success" role="alert">
                    {!! \Session::get('success') !!}
                    <span class="closebtn" onclick="this.parentElement.style.display='none';">&times;</span>
                </div>
            </div>
        </div>
    @endif

    <div class="action_btn">
        <div class="text-center conoflinks">


            <form method="post" action="{{ route('search_courses') }}">
                @csrf
                <input type="text" name="search_key" placeholder="Search..">
                <button type="submit" value="Submit">submit</button>
            </form>
        </div>
    </div>
<br>

    <div class="conoflinks">
        <table style="width:60%" >
            <tr>
                <th>name</th>
                <th>code</th>
                <th>Description</th>
                <th>view Details</th>
            </tr>
            @foreach($courses as $course)
            <tr>
                <td>{{ $course->name }}</td>
                <td>{{ $course->code }}</td>
                <td>{{ $course->description }}</td>
                <td>
                    <a href="{{ route('show_courses', $course->id) }}"> View details </a>

                </td>
            </tr>
            @endforeach
        </table>
    </div>
@endsection
