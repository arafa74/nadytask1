@extends('layouts.app')

@section('content')

    <style>
        table, th, td {
            border:1px solid black;
        }

        .action_btn {
            width: 200px;
            margin: 0 auto;
            display: inline;}
        .conoflinks {
            justify-content: center;
            display: flex;
            align-items: center;
        }
        .navbar-brand{
            margin: 10px;
            font-size: 20px;
            font-weight: bold;
        }
    </style>
    @if (\Session::has('success'))
        <div class="conoflinks">
            <div class="col-10">
                <div class="alert alert-success" role="alert">
                    {!! \Session::get('success') !!}
                    <span class="closebtn" onclick="this.parentElement.style.display='none';">&times;</span>
                </div>
            </div>
        </div>
    @endif

    <div class="action_btn">
        <div class="text-center conoflinks">
            <a class="navbar-brand" href="{{ url('/students_create') }}">
                Add New Student
            </a>

            <form method="post" action="{{ route('search_students') }}">
                @csrf
                <input type="text" name="search_key" placeholder="Search..">
                <button type="submit" value="Submit">submit</button>
            </form>
        </div>
    </div>
<br>

    <div class="conoflinks">
        <table style="width:60%" >
            <tr>
                <th>name</th>
                <th>code</th>
                <th>email</th>
                <th>Delete</th>
            </tr>
            @foreach($students as $student)
            <tr>
                <td>{{ $student->name }}</td>
                <td>{{ $student->code }}</td>
                <td>{{ $student->email }}</td>
                <td>

                    <a href="#" data-href="delete.php?id={{ $student->id }}" data-toggle="modal" data-target="#confirm-delete">Delete</a>

                    <div class="modal fade" id="confirm-delete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    Are You Sure ?
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                                    <a href="{{ route('delete_student', $student->id) }}" class="btn btn-danger btn-ok">Delete</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </td>
            </tr>
            @endforeach
        </table>
    </div>
@endsection
